<?php
require_once './src/app/Autoloader.php';

use Controllers\UserController;

$users = UserController::viewUsers();

require_once './src/templates/usuarios.template.php';
