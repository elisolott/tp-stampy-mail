<?php
require_once './src/app/Autoloader.php';

use Controllers\UserController;

UserController::deleteUser();

header('Location: ./usuarios.php');
