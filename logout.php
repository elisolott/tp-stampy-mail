<?php
require_once './src/app/Autoloader.php';

use Controllers\LoginController;

LoginController::logout();

header('Location: ./login.php');
