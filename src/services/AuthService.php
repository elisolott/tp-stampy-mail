<?php
namespace Services;

use \PDO;
use App\DbConnection;
use Models\User;

class AuthService {
    private $connection = null;

    function __construct() {
        $this->connection = DbConnection::connect();
    }

    function authorizeUser ($username, $password) {
        if (is_null($username) || is_null($password)) {
            return false;
        }

        $sql = 'SELECT * FROM users WHERE username = :username AND password = :password';

        $stmt = $this->connection->prepare($sql);

        $stmt->execute([
            ':username' => $username,
            ':password' => $password,
        ]);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$row) {
            return false;
        }

        $user = new User($row['username'], null, $row['email'], $row['first_name'], $row['last_name'], $row['id'], $row['created_at'], $row['updated_at']);
        
        return $user;
    }

    function verifyUser ($username, $email) {
        if (is_null($username) || is_null($email)) {
            return false;
        }

        $sql = 'SELECT * FROM users WHERE username = :username AND email = :email';

        $stmt = $this->connection->prepare($sql);

        $stmt->execute([
            ':username' => $username,
            ':email' => $email,
        ]);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$row) {
            return false;
        }

        $user = new User($row['username'], null, $row['email'], $row['first_name'], $row['last_name'], $row['id'], $row['created_at'], $row['updated_at']);
        
        return $user;
    }
}
