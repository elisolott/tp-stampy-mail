<?php
namespace Services;

use \PDO;
use App\DbConnection;
use Models\User;

class UserService {
    private $connection = null;

    function __construct() {
        $this->connection = DbConnection::connect();
    }

    function getOneUser ($id) {
        $sql = 'SELECT * FROM users WHERE id = :id';

        $stmt = $this->connection->prepare($sql);

        $stmt->execute([
            ':id' => $id
        ]);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $getOneUser = new User($row['username'], null, $row['email'], $row['first_name'], $row['last_name'], $row['id']);

        return $getOneUser;
    }

    function getAllUsers () {
        $sql = 'SELECT * FROM users';

        $stmt = $this->connection->prepare($sql);

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $allUsers = [];

        foreach ($rows as $row) {
            $user = new User($row['username'], null, $row['email'], $row['first_name'], $row['last_name'], $row['id']);
            array_push($allUsers, $user);
        }

        return $allUsers;
    }

    function insertUser ($newUser) {
        $sql = 'INSERT INTO users (username, password, email, first_name, last_name) VALUES
        (:username, :password, :email, :first_name, :last_name)';

        $stmt = $this->connection->prepare($sql);

        $stmt->execute([
            ':username' => $newUser->getUsername(),
            ':password' => $newUser->getPassword(),
            ':email' => $newUser->getEmail(),
            ':first_name' => $newUser->getFirstName(),
            ':last_name' => $newUser->getLastName()
        ]);

        $newUser = $stmt->fetch(PDO::FETCH_ASSOC);

        return $this->connection->insertUserId;
    }

    function updateUser ($updateUser) {
        $sql = 'UPDATE users SET username = :username, email = :email, first_name = :first_name, last_name = :last_name WHERE id = :id';

        $stmt = $this->connection->prepare($sql);

        $stmt->execute([
            ':username' => $updateUser->getUsername(),
            ':email' => $updateUser->getEmail(),
            ':first_name' => $updateUser->getFirstName(),
            ':last_name' => $updateUser->getLastName(),
            ':id' => $updateUser->getId()
        ]);

        $updateUser = $stmt->fetch(PDO::FETCH_ASSOC);

        return $updateUser;
    }

    function deleteUser ($id) {
        $sql = 'DELETE FROM users WHERE id = :id';

        $stmt = $this->connection->prepare($sql);

        $stmt->execute([
            ':id' => $id
        ]);

        $deteleOneUser = $stmt->fetch(PDO::FETCH_ASSOC);

        return $deteleOneUser;
    }
}
