<?php
namespace App;

use App\Logger;
use Services\AuthService;

class UserSession {
    static function login($username, $password) {
        session_start();

        $authService = new AuthService();
        $user = $authService->authorizeUser($username, $password);

        if (!$user) {
            Logger::logError("Error al autorizar al usuario.");
            return false;
        }

        $_SESSION['username'] = $user->getUsername();
        $_SESSION['email'] = $user->getEmail();
        
        return true;
    }

    static function logout() {
        session_start();
        session_destroy();
    }

    static function verify() {
        session_start();

        if (!isset($_SESSION['username']) || !isset($_SESSION['email'])) {
            session_destroy();
            header('Location: ./login.php');
            return false;
        }

        $authService = new AuthService();
        $user = $authService->verifyUser($_SESSION['username'], $_SESSION['email']);

        if (!$user) {
            session_destroy();
            header('Location: ./login.php');
            return false;
        }

        return true;
    } 
}
