<?php
namespace App;

use \PDO;

class DbConnection {
    static function connect() {
        $dsn = "mysql:host=localhost;dbname=tp_stampy_mail;charset=UTF8";

        $connection = new PDO($dsn, 'root', '');

        return $connection;
    }
}
