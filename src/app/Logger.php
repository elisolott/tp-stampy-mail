<?php
namespace App;

class Logger {
    static function logError($mensaje){
        $fecha = date('Y-m-h H:i');
        
        $filePointer = fopen(realpath(dirname(__FILE__) . '/error_log'),'a+');
        fwrite($filePointer, "$fecha - [ERROR] - $mensaje\n\r");
        fclose($filePointer);
    }
}
