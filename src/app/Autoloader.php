<?php

function loadClasses ($className) {
    $useSeparator = str_replace('\\', DIRECTORY_SEPARATOR, $className);
    $separateNamespace = explode("/", $useSeparator);

    $path = strtolower($separateNamespace[0]);
    $file = "$separateNamespace[1].php";

    $fullPath = './src'.DIRECTORY_SEPARATOR.$path.DIRECTORY_SEPARATOR.$file;

	if (file_exists($fullPath)) {
		require $fullPath;
	}
}

spl_autoload_register('loadClasses');
