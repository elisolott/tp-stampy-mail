<?php
namespace Models;

class User {
    private $id = null;
    private $username = null;
    private $password = null;
    private $email = null;
    private $firstName = null;
    private $lastName = null;
    private $createdAt = null;
    private $updatedAt = null;

    function __construct(
        $username, $password, $email, $firstName, $lastName,
        $id = null, $createdAt = null, $updatedAt = null
    ) {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getUsername () {
        return $this->username;
    }
    
    public function setUsername ($username) {
        $this->username = $username;
    }

    public function getPassword () {
        return $this->password;
    }

    public function setPassword ($password) {
        $this->password = $password;
    }

    public function getEmail () {
        return $this->email;
    }

    public function setEmail ($email) {
        $this->email = $email;
    }

    public function getFirstName () {
        return $this->firstName;
    }

    public function setFirstName ($firstName) {
        $this->firstName = $firstName;
    }

    public function getLastName () {
        return $this->lastName;
    }

    public function setLastName ($lastName) {
        $this->lastName = $lastName;
    }

    public function getCreatedAt () {
        return $this->createdAt;
    }

    public function setCreatedAt ($createdAt) {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt () {
        return $this->updatedAt;
    }

    public function setUpdatedAt ($updatedAt) {
        $this->updatedAt = $updatedAt;
    }
}
