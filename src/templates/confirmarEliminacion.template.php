<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./src/public/css/style.css" type="text/css" />
    <title>Confirmar eliminación</title>
</head>
<body>
    <h1>Gestión de usuarios</h1>
    <ul class="menu">
        <li><a href="./usuarios.php">Listado</a></li>
        <li><a href="./crearUsuario.php">Crear</a></li>
        <li><a href="./logout.php">Logout</a></li>
    </ul>
    <h2>¿Desea eliminar al usuario con el ID <?php echo $id; ?>?</h2><br/>
    <a class="boton-aceptar" href="./eliminarUsuario.php?id=<?php echo $id; ?>">Si</a>
    <a class="boton-eliminar" href="./usuarios.php">Cancelar</a>
</body>
</html>
