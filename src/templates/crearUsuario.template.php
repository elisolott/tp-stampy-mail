<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./src/public/css/style.css" type="text/css" />
    <title>Crear un nuevo usuario</title>
</head>
<body>
    <h1>Gestión de usuarios</h1>
    <ul class="menu">
        <li><a href="./usuarios.php">Listado</a></li>
        <li><a href="./crearUsuario.php">Crear</a></li>
        <li><a href="./logout.php">Logout</a></li>
    </ul>
    <h3>Crear nuevo usuario</h3>
    <form action="./guardarNuevoUsuario.php" method="post">
        <label>Usuario</label><input type="text" name="username" /><br />
        <label>Contraseña</label><input type="password" name="password" /><br />
        <label>Email</label><input type="text" name="email" /><br />
        <label>Nombre</label><input type="text" name="firstName" /><br />
        <label>Apellido</label><input type="text" name="lastName" /><br />
        <button>Guardar</button>
    </form>
</body>
</html>
