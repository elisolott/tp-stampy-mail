<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./src/public/css/style.css" type="text/css" />
    <title>Lista de usuarios</title>
</head>
<body>
    <header>
        <h1>Gestión de usuarios</h1>
        <ul class="menu">
            <li><a href="./usuarios.php">Listado</a></li>
            <li><a href="./crearUsuario.php">Crear</a></li>
            <li><a href="./logout.php">Logout</a></li>
        </ul>
        <table width="100%" table-layout="fixed" cellpadding="8">
            <thead>
                <tr>
                    <th>Usuario</th>
                    <th>Email</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    foreach ($users as $user) {
                        echo "
                            <tr>
                                <td>{$user->getUsername()}</td>
                                <td>{$user->getEmail()}</td>
                                <td>{$user->getFirstName()}</td>
                                <td>{$user->getLastName()}</td>
                                <td>
                                    <a class='boton-editar' href='./editarUsuario.php?id={$user->getId()}'>Editar</a>
                                    <a class='boton-borrar' href='./confirmarEliminacion.php?id={$user->getId()}'>Eliminar</a>
                                </td>
                            </tr>
                        ";
                    }
                ?>
            </tbody>
        </table>
    </header>
</body>
</html>
