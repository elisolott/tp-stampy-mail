<?php
namespace Controllers;

use App\Logger;
use Models\User;
use Services\UserService;
use App\UserSession;


class LoginController {
    static function login () {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $loginOk = UserSession::login($username, $password);

        if ($loginOk) {
            header('Location: ./usuarios.php');
            return;
        } else {
            header('Location: ./login.php');
            return;
        }
    }

    static function logout () {
        UserSession::logout();
    }
}
