<?php
namespace Controllers;

use App\Logger;
use Models\User;
use Services\UserService;
use App\UserSession;


class UserController {
    static function viewUsers () {
        $userOk = UserSession::verify();

        if (!$userOk) {
            return;
        } 

        $userService = new UserService();

        $users = $userService->getAllUsers();

        return $users;
    }

    static function viewUser () {
        $userOk = UserSession::verify();

        if (!$userOk) {
            return;
        }

        $id = $_GET['id'];

        $userService = new UserService();

        $user = $userService->getOneUser($id);

        return $user;
    }

    static function saveUser () {
        $userOk = UserSession::verify();

        if (!$userOk) {
            return;
        }

        $id = $_POST['id'];

        $userService = new UserService();

        $user = $userService->getOneUser($id);

        $user->setUsername($_POST['username']);
        $user->setEmail($_POST['email']);
        $user->setFirstName($_POST['firstName']);
        $user->setLastName($_POST['lastName']);

        $userService->updateUser($user);
        
        return true;
    }

    static function promptUser () {
        $userOk = UserSession::verify();

        if (!$userOk) {
            return;
        }

        $id = $_GET['id'];

        return $id;
    }

    static function deleteUser () {
        $userOk = UserSession::verify();

        if (!$userOk) {
            return;
        }

        $id = $_GET['id'];

        $userService = new UserService();
        
        $userService->deleteUser($id);

        return true;
    }

    static function insertUser () {
        $userOk = UserSession::verify();

        if (!$userOk) {
            return;
        }

        $userService = new UserService();
        
        $user = new User($_POST['username'], $_POST['password'], $_POST['email'], $_POST['firstName'], $_POST['lastName']);

        $userService->insertUser($user);

        return true;
    }

    static function createUser () {
        $userOk = UserSession::verify();

        if (!$userOk) {
            return;
        }
    }
}
